import { IsNotEmpty } from 'class-validator';

export class CreateOrderItemDto {
  productId: number;

  amount: number;
}
export class CreateOrderDto {
  @IsNotEmpty()
  customerId: number;
  orderItems: CreateOrderItemDto[];
}

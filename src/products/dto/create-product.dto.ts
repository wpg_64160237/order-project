import { MinLength, IsPositive, IsNotEmpty } from 'class-validator';

export class CreateProductDto {
  @IsNotEmpty()
  @MinLength(2)
  name: string;

  @IsNotEmpty()
  @IsPositive()
  price: number;
}
